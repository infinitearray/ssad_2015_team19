Project Title:
==============
Crawl data, Build warehouse and API for Food, Travel, Groceries, Movies, Hotels, Shopping data, crawled from different websites.

Team Name: Crawlers
SSAD Team No : 19
Team Memebers : C.G.Vedant, Nynaru Vivek, Bandi Prasanna Harsha Vardhan, Sane Koushik Reddy
Client : Nitesh Surtani, SuperGenie Technologies
TA : Shivam Khandelwal

DESCRIPTION:
=============
SuperGenie is an on demand personal assistant for ecommerce. Buying a product or a service online is pain caused by the number of choices available and the decisions that the user has to take. SuperGenie will allow users to place orders via Chat and Speech for Food, Travel, Groceries, Shopping, Hotel Booking etc.

As a part of the project :
1. Crawl data from different websites or use their API for a specific category. (Example, Zomato and FoodPanda for Food).
2. Build a Warehouse by combining the data from different sources into one that will be used as a dictionary for NLP engine and for Search.
3. Build a REST Api to allow different modules to access this data.


Technologiees required:
============================
Python libraries:	bs4,requests,MySQLdb
Phatomjs
MySQL


Usage - Books:
===============
(Mysmartprice)
To change the mysql admin credentials, they can be found in the file "db_connection.py"

To update the database Run the commands:
---------------------------------------
	python db_book.py
	python db_brstprice.py
	python db_reviews.py

To get the details of a particular book:
------------------------------------------
	Find the book from the database with the known details of the book and select the url of the book

Run the following commands:
For details:	python eachbook.py	input:<url>
For reviews:	python reviews.py	input:<url>
For bestprice:	phantomjs bestprice.js <url>

Note:	Code to crawl books from Amazon and Flipkart are also included, they might be useful to the client.
	Amazon is limiting the no.of books to 1200 with any filters.So we have used the low-price and high-price filters and binary searched for the prices and got thhe ranges of prices.Now the part that is left is to get the reviews of each book and iterate through the levels (i.e get all categories,get ranges of prices,get list of books in a particular range,get details of each book codes are written for each) and store them in the database.
	Flipkart is also limiting the no.of books to 1500 with any filters.But we are unable to find any filter so that we can get the list of all books from flipkart.So the codes written are basically general versions of scripts to get list from a given page.

Usage - Mobile :
===============
(Mysmartprice)
To change the mysql admin credentials, they can be found in the file "config.ini".
Two modules have been provided. One for crawling data and another of inserting the crawled data.

Instructions to crawl data:
---------------------------------------
	Found in mobile_crawl.py
	Use getMobileList to get the list of all mobiles in mysmartprice.com
	SearchMobile can also be used to search for a particular mobile
	The url obtained above is then used to add the mobile info to database
	

Instructions to access database:
------------------------------------------
	Make sure config.ini is configured correctly.
	If the tables are not present, use setupTables to create them.
	Use addMobile along with the url to add the mobile to the database.

Note:	Most of the fields in a table are strings are not very useful while writing queries. Please refer to the database schema to see what data the columns store.
	To speed up the whole process, Duplicate entries(to vendor and mobile) are avoided. Therefore to insert new data with same primary key, the old entry must first be deleted.

Usage - Movies:
===============
(BookMyShow)

Initialisation of database can be done by running mysql file database_initialize.sql
To change the mysql admin credentials, the files database_initialize.sql, requirements.py must be changed

To update the database Run the commands:
---------------------------------------
	python push_cities_into_db.py
	python push_movies_and_cinemas_into_db.py
	python push_reviews_into_db.py
	python push_class_mapping_into_db.py

For inquiring about availability of movie Run some of the following commands:
------------------------------------------
        If movie is fixed                     :   python get_cinemas_of_movie.py
	If cinema is fixed                    :   python get_movies_of_cinemas.py
	If movie and cinema are fixed         :   python get_shows_by_movie_and_cinema.py
	If details about classes are required :   python get_shows_by_movie_and_cinema_with_classes.py
