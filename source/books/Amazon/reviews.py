import bs4
from os import popen
import socket
import collections as Col
import requests

import re


def get_reviews(url):
    array = dict()
    dates = []
    array['author'] = array['review'] = array['date'] = []
    f = requests.get(url)
    html = f.text
    soup = bs4.BeautifulSoup(html)
    d = dict()
    # for all details of book
    for data in soup.find_all("div", {'class': 'a-row'}):
        for val in data("div", {'id': 'revMHRL'}):
            for x in val("div", {'class': 'a-section celwidget'}):
                for y in x("span", {'class': 'a-size-normal'}):
                    for z in y("a"):
                        array['author'].append(str(z.text.strip()))
                for z in x("span", {'class': 'a-color-secondary'}):
                    if (z.nextSibling):
                        continue
                    dates.append(str(z.text.strip()))
            for x in val("div", {'class': 'a-section'}):
                print x.text.strip()
    for i in range(len(dates)):
        if ((i + 1) % 2 == 0):
            array['date'].append(dates[i])
    return array


a = get_reviews(raw_input())
