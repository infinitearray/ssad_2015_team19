from bs4 import BeautifulSoup
from os import popen
import socket
import collections as Col
import requests

from re import sub
from cat import *

main_url = "http://amazon.in"


def get_pagelinks(url):
    f = requests.get(url)
    html = f.text
    soup = BeautifulSoup(html)
    link = ""
    for data in soup.find_all("span", {'class': 'pagnRA'}):
        for var in data("a"):
            link = str(var['href'])
        #		print str(data.text.strip())
    return link


def get_no_of_pages(url):
    f = requests.get(url)
    html = f.text
    soup = BeautifulSoup(html)
    cnt = 0
    for data in soup.find_all("span", {'class': 'pagnDisabled'}):
        cnt = str(data.text.strip())
    return cnt


ctg = get_categories(main_url + "/books")
ctg = Col.OrderedDict(sorted(ctg.items()))
for i in ctg:
    print i, get_no_of_pages(main_url + ctg[i]), get_pagelinks(main_url + ctg[i])
