from bs4 import BeautifulSoup
from os import popen
import socket
import collections as Col
import requests

from re import sub


def get_count(url):
    f = requests.get(url)
    html = f.text
    soup = BeautifulSoup(html)
    var = ""
    it = -3
    qw = 1
    for data in soup.find_all('h2', {'id': 's-result-count'}):
        var = str(data.text.strip().encode('utf-8'))
    for i in range(len(var)):
        if (var[i] == 'o' and var[i + 1] == 'f'):
            it = i
            break
    for i in range(len(var)):
        if (var[i] == 'r' and var[i + 1] == 'e'):
            qw = i
            break
    s = var[it + 3:qw - 1]
    #	print "string..",s
    if (s == ''):
        return 0
    return int(s.replace(',', ''))


print "Enter URL:"
link = raw_input()
lowval = 0.0
maxdiff = 0
sum_count = 0
total_count = get_count(link)
while sum_count < total_count:
    maxdiff = 100.0
    count = 1300
    while count > 1200:
        maxdiff /= 2
        it = 0
        qw = 0
        count = get_count(link + "&low-price=%d" % (lowval) + "&high-price=%d" % (lowval + maxdiff))
    print lowval, lowval + maxdiff, count
    lowval += maxdiff + 1
    sum_count += count
