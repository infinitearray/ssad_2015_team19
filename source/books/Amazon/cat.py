from bs4 import BeautifulSoup
from os import popen
import socket
import collections as Col
import requests

from re import sub


def get_categories(url):
    array = dict()
    f = requests.get(url)
    html = f.text
    soup = BeautifulSoup(html)
    # for the details
    cnt = 0
    for data in soup.find_all("div", {'class': 'categoryRefinementsSection'}):
        for val in data("li"):
            link = ""
            name = ""
            for var in val("a"):
                link = str(var['href'])
            for var in val("span", {'class': 'refinementLink'}):
                name = str(var.text.strip())
            if cnt >= 3:
                array[name] = link
            cnt += 1

    return array


"""list1 = get_categories("http://www.amazon.in/books")
list1 = Col.OrderedDict(sorted(list1.items()))
for i in list1:
	print i,list1[i]"""
