from bs4 import BeautifulSoup
from os import popen
import socket
import collections as Col
import requests

from re import sub


def get_booklist(url):
    array = keys = []
    f = requests.get(url)
    html = f.text
    soup = BeautifulSoup(html)
    d = dict()
    # for all details of book
    for data in soup.find_all("div", {'id': 'a-page'}):
        for val in data("div", {'class': 'content'}):
            for val1 in val("ul"):
                for var in val1("li"):
                    if var("script") or var("a"):
                        continue
                    for x in var("b"):
                        array.append(str(x.text.strip()))
                    array.append(str(var.text.strip().encode('utf-8')))
    # for title
    for data in soup.find_all("div", {'id': 'a-page'}):
        for val in data("span", {'id': 'productTitle'}):
            d['title'] = str(val.text.strip())
    for i in range(0, len(array), 2):
        d[array[i][:-1]] = array[i + 1][len(array[i]) + 1:].strip()
    # for price of book
    for data in soup.find_all("div", {'id': 'a-page'}):
        for val in data("span", {'class': 'a-color-base'}):
            for var in val("span", {'class': 'a-color-price'}):
                d['price-1'] = str(var.text.strip())
    arr = []
    for data in soup.find_all("div", {'id': 'a-page'}):
        for val in data("span", {'class': 'a-color-secondary'}):
            for var in val("span"):
                arr.append(str(var.text.strip()))
    for i in arr:
        if i:
            d['price-2'] = i
            break
    return d


A = get_booklist(raw_input())
A = Col.OrderedDict(sorted(A.items()))
for i in A:
    print i, ":", A[i]
