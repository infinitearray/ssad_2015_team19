from bs4 import BeautifulSoup
from os import popen
import socket

import requests
import collections
from re import sub
from categories import *

def get_subcategories(url):
	array = dict()
	f = requests.get(url)
	html = f.text
	soup = BeautifulSoup(html)
	#for the details 
	for data in soup.find_all("div",{'class':'nav-section-cat-list'}):
		for val in data("a"):
			array[str(val.text.strip())] = str(val['href'])
	
	return array

subcat=catlist= dict()
catlist = get_categories("http://www.flipkart.com/books")
catlist = collections.OrderedDict(sorted(catlist.items()))

for i in catlist:
	print "\n",i,catlist[i],"\n"
	subcat = get_subcategories("http://www.flipkart.com"+catlist[i])
	subcat = collections.OrderedDict(sorted(subcat.items()))
	for j in subcat:
		print j,":",subcat[j]
	if len(subcat)==0:
		print "No subcategories !!"
	
