from bs4 import BeautifulSoup
from os import popen
import socket

import requests

from re import sub



def get_categories(url):
	array = dict()
	f = requests.get(url)
	html = f.text
	soup = BeautifulSoup(html)
	#for the details 
	for data in soup.find_all("div",{'class':'scrollable menu-content'}):
		for val in data("li"):
			link =""
			for var in val("a"):
				link = str(var['href'])	
			array[str(val.text.strip())] = link
	
	return array

#print get_categories("http://www.flipkart.com/books")
	


