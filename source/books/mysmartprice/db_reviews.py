from reviews import *
import db_connection
# creating the table reviews
db_connection.cursor.execute(
    """CREATE TABLE IF NOT EXISTS reviews(ISBN varchar(100) NOT NULL, Author varchar(100) NOT NULL, Date varchar(100) NOT NULL, Rating varchar(100) NOT NULL, Review varchar(2000) NOT NULL);""")
# get isbn values from the table main
db_connection.cursor.execute("""SELECT ISBN from main;""")
isbn_vals = db_connection.cursor.fetchall()
# to get reviews of each of the book identified by isbn values
for isbn in isbn_vals:
    links = get_links(isbn[0])
    for url in links:
        text = get_review(url)
        try:
            db_connection.cursor.execute("INSERT INTO reviews VALUES(%s,%s,%s,%s,%s);",
                           (isbn[0], text['author'], text['date'], text['rating'], text['review']))
            db_connection.db.commit()
        except:
            db_connection.db.rollback()

db_connection.db.close()
