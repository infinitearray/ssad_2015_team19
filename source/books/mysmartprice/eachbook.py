import bs4
import requests
import collections


def get_details_book(url):
    keys = []
    values = []
    details = dict()
    searchurl = url
    about = []

    load_url = requests.get(searchurl)
    html = load_url.text
    soup = bs4.BeautifulSoup(html)
    # for the details
    for data in soup.find_all("td", {'class': 'specsKey'}):
        keys.append(str(data.text.strip().encode('utf-8')))

    for data in soup.find_all("td", {'class': 'specsValue'}):
        values.append(str(data.text.strip().encode('utf-8')))
    # for the image
    keys.append('Image')
    for data in soup.find_all("img", {'id': 'book_img'}):
        values.append(str(data['src'].encode('utf-8')))
    # About the book
    keys.append('About the Book')
    for data in soup.find_all("div", {'class': 'section_title'}):
        about.append(str(data.text.strip().encode('utf-8')))
    values.append(about[2])
    for i in range(len(keys)):
        details[keys[i]] = values[i]
    return details

if __name__ == "__main__":
    print "Enter the url of the book:"
    url = raw_input()
    print "\n\n"
    details = get_details_book(url)
    details = collections.OrderedDict(sorted(details.items()))
    for i in details:
        print i,":",details[i],"\n"
