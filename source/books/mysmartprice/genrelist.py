import bs4
import requests


# to get the all available genres from mysmartprice
def get_genrelist(url):
    array = []
    urls = []
    f = requests.get(url)
    html = f.text
    soup = bs4.BeautifulSoup(html)

    for data in soup.find_all("ul", {'class': 'sitemap page_listing clearfix'}):
        for val in data("li"):
            array.append(str(val.text.strip()))
        for val in data("a"):
            urls.append(str(val['href']))
    dic = dict()
    for i in range(len(array)):
        dic[array[i]] = urls[i]

    return dic
