import MySQLdb
import warnings

username = "books_admin"
password = "supergenie"
database = "mysmartprice_books"
server = "localhost"

db = MySQLdb.connect(server, username, password, database)
cursor = db.cursor()
# to ignore mysql warnings
warnings.filterwarnings('ignore', category=MySQLdb.Warning)
