import bs4
import os
import db_connection

# creating the table prices
db_connection.cursor.execute("""CREATE TABLE IF NOT EXISTS prices(ISBN VARCHAR(100),Price VARCHAR(100));""")

# get the links of all books from main table
db_connection.cursor.execute("SELECT Link from main;")
links = db_connection.cursor.fetchall()

# get all isbn values of books from main table
db_connection.cursor.execute("SELECT ISBN from main;")
isbn = db_connection.cursor.fetchall()

for i in range(len(links)):
    url = links[i][0]
    price = ""
    cnt = 0
    while (len(price) <= len("Best Price:")):
        if (cnt == 3):  # to make sure that the phantomjs code loads the best price of the given book
            cnt = -1
            price = "N/A"  # assigning nll value to price if it is not fetched properly
            break
        os.system("phantomjs bookprice.js " + url + " >/dev/null")
        html = open("./page.html", 'r').read()
        soup = bs4.BeautifulSoup(html)
        os.system("rm ./page.html")
        cnt += 1
        price = str(soup.find("div", {'class': 'bestprice'}).text.strip())
    if cnt != -1:
        price = price[12:]
    try:
        # push to database
        db_connection.cursor.execute("INSERT INTO prices(ISBN,Price) VALUES(%s,%s);", (isbn[i][0], price))
        db_connection.db.commit()
    except:
        db_connection.db.rollback()
db_connection.db.close()
