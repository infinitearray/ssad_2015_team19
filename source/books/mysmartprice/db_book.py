import collections

import genrelist
import booklist
import eachbook
import db_connection

# creating the table main
db_connection.cursor.execute(
    """CREATE TABLE IF NOT EXISTS main(ISBN varchar(100) NOT NULL, Title varchar(100) NOT NULL, Link varchar(100) NOT NULL, Price varchar(100) NOT NULL, Author varchar(100) NOT NULL, Publisher varchar(100) NOT NULL, Pages varchar(100) NOT NULL, PublicationYear varchar(100) NOT NULL, Language varchar(100) NOT NULL, Binding varchar(100) NOT NULL,PRIMARY KEY(ISBN));""")
# get all genres from mysmartprice
genres = genrelist.get_genrelist('http://mysmartprice.com/books/genres')
genres = collections.OrderedDict(sorted(genres.items()))
# for each genre iterate
for i in genres:
    # get books in each genre
    books = booklist.get_booklist(genres[i])
    for j in range(len(books)):
        details = dict()
        # get details of each book
        details = eachbook.get_details_book(books[j]["url"])
        try:
            # pushing them into database
            db_connection.cursor.execute(
                "INSERT INTO main(ISBN, Title, Link, Price, Author, Publisher, Pages, PublicationYear, Language, Binding) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);",
                (details['ISBN'], books[j]['title'], books[j]['url'], books[j]['price'], details['Author'],
                 details['Publisher'], details['Number of Pages'], details['Publication Year'], details['Language'],
                 details['Binding']))
            db_connection.db.commit()
        except:
            db_connection.db.rollback()
db_connection.db.close()
