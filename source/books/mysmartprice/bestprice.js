var page = require('webpage').create(),
    system = require('system'),
    address,
    fs = require('fs'),
    outfile = './page.html'

address = system.args[1];
page.onLoadFinished = function() {
	fs.write(outfile,page.content,'w');
	phantom.exit();
};

page.open(address,function(status){});
