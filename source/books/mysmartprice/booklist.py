import bs4
import requests


def get_booklist(url):
    array = []
    load_url = requests.get(url)
    html = load_url.text
    soup = bs4.BeautifulSoup(html)

    for data in soup.find_all("a"):
        if (data.has_attr('title') and data.has_attr('id')):
            dic = dict()
            dic["id"] = str(data['id'])
            dic["url"] = str(data['href'])
            dic["title"] = str(data['title'].encode('utf-8'))
            for val in data("div", {'class':'book_author'}):
                dic["author"] = str(val.text.strip())
            for val in data("div", {'class': 'book-price'}):
                dic["price"] = str(val.text.strip())
            array.append(dic)

    return array
