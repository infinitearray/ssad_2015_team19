drop database if exists mysmartprice_books;
flush privileges;
create database mysmartprice_books;
grant all privileges on mysmartprice_books.* to 'books_admin'@'localhost' identified by 'supergenie';
