# -*- coding: utf-8 -*-
import bs4
import requests
import collections


# to get the links of all reviews of a given book(isbn)
def get_links(isbn):
    array = []
    urls = []
    url = "http://www.goodreads.com/api/reviews_widget_iframe?did=0&isbn="
    url += str(isbn)
    f = requests.get(url)
    html = f.text
    soup = bs4.BeautifulSoup(html)

    for data in soup.find_all("div", {'class': 'gr_review_container'}):
        for var in data("span", {'class': 'gr_review_by'}):
            for val in var("a"):
                array.append(val['href'])

    return array


# to get the reviews in current page of the given url of a book
def get_review(link):
    f = requests.get(link)
    html = f.text
    soup = bs4.BeautifulSoup(html)
    array = dict()
    review = ""

    for data in soup.find_all("div", {'class': 'content'}):
        for val in data("div", {'class': 'reviewText mediumText description'}):
            array['review'] = str(val.text.strip().encode('utf-8'))
        for val in data("a", {'class': 'staticStars'}):
            array['rating'] = str(val.text.strip().encode('utf-8'))
        for val in data("a", {'class': 'userReview'}):
            array['author'] = str(val.text.strip().encode('utf-8'))
        for val in data("span", {'itemprop': 'publishDate'}):
            array['date'] = str(val.text.strip().encode('utf-8'))
    return array

if __name__ == "__main__":
    print "Enter the url of the book:"
    url = raw_input()
    print "\n"
    isbn = url[url.find("=")+1:]
    print isbn
    links = get_links(isbn)
    for i in links:
        review = get_review(i)
        for i in review:
            print i,":",review[i],"\n"
        print"-----------------------"