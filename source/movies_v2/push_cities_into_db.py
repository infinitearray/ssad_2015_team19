#! /usr/bin/env python

from requirements import cursor, db
from get_city import cities

# Dropping table in case it exists

try :
    cursor.execute("DROP TABLE city")
except :
    pass

# Creating tables

cursor.execute("CREATE TABLE city (name VARCHAR(30) NOT NULL,code VARCHAR(20) NOT NULL,state VARCHAR(20) NOT NULL,url VARCHAR(60) NOT NULL)")

# Inserting data into tables

try :
    cursor.executemany("INSERT INTO city (name,code,state,url) VALUES (%s,%s,%s,%s)",cities)
    db.commit()
except :
    db.rollback()

db.close()
