#! /usr/bin/env python

from requirements import get, Soup, loads, msplit, sub, system, cursor
import time
from subprocess import Popen,PIPE

current_date = time.strftime("%d/%m/%Y")
date = "".join(current_date.split('/')[ : : -1])

sql = "SELECT name,url FROM cinema"
cursor.execute(sql)                                                 # Querying database to get list of theatres
theatres = cursor.fetchall()
classes = ()

cnt=0
for theatre in theatres:

	cnt+=1
	name = theatre[0]                                           # Name of theatre
	url = theatre[1] + date                                     # url of theatre
	code = url.split('-')[-2]
	sp = Popen("phantomjs new.js '" + url + "'", shell = True, stdout = PIPE, stderr = PIPE)
	out, err=sp.communicate()
	if len(out) > 0 :
		print cnt,url
		continue
	print cnt
	page = open('./page.html').read()
	html = Soup(page, "lxml")

	# Iterating over html using required tags

	for li in html.findAll('li', {'class' : 'list'}) :

		for div in li.findAll('div', {'class' : 'body   __cn-list'}) :

			for div2 in div.findAll('div') :

				for a in div2.findAll('a') :

					url = "http://in.bookmyshow.com/serv/getData?cmd=GETSHOWINFO&vid=" + \
							code + "&ssid=" + a['onclick'].split(',')[1].replace('\'', '')
							            # url storing list of class details of a cinema
					data = loads(get(url).content[12 : -1])
					                            # Loading JSON of class details
					ini = 64 + len(data)        # Stores class mapping of current class
					for category in data :
						classes += ((name, category[-8], str(chr(ini)), category[4], ), )
						ini -= 1
				break
			break
		break
