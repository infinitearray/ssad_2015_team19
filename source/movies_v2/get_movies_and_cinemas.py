#! /usr/bin/env python

from requirements import cursor, get, loads

URL = 'http://in.bookmyshow.com/serv/getData?cmd=QUICKBOOK&type=MT'                         # URL to get list of movies and cinemas in city 
movies = ()
cinemas = ()

cursor.execute("SELECT * FROM city")                                                        # Querying database to get list of all cities
cities = cursor.fetchall()                                                                  # Fetching details of all cities

# Iterating over all cities
for city in cities :

    code = city[1]                                                                          # Storing city code
    name = city[0].title()                                                                  # Storing city name in Title format
    cookie = { 'Rgn' : '%7CCode%3D' + '%s'%(code) + '%7Ctext%3D' + '%s'%(name) + '%7C' }    # Cookie required to get details of a preferred city
    page = get(URL, cookies=cookie).content
    data = loads(page)                                                                      # Loading JSON content in the page
    
    try :
        # Iterating over all movies in data
	for movie in data['moviesData']['BookMyShow']['arrEvents'] :
            for part in movie['ChildEvents'] :

                # List of Details of a movie 
		movie_name = part['EventName']
                dimension = part['EventDimension']
                censor = part['EventCensor']
                movie_code = part['EventCode']
                rating = str(part['EventRating'])
                release = str(part['EventDate'])
                if censor == "" :                                                           # Formatting censor to sustain uniformity
                    censor = "-"
                url = "http://in.bookmyshow.com/buytickets/"+part['EventURL']+"-"+city[0]+"/movie-"+code+"-"+part['EventCode']+"-MT/"
		                                                                            # url for specific movie
                language = part['EventLanguage'].split(' ')[0]
                movies += ((movie_name, url, dimension, censor, release, language, movie_code), )  
		                                                                            # Appending details of new movie into list
    except :

        pass
    
    try :
	# Iterating over all theatres in data
        for theatre in data['cinemas']['BookMyShow']['aiVN'] :
	    
            # List of Details of a cinema(theatre)
            name = theatre['VenueName'].replace('/', '').replace(' ', '-').replace(':', '').replace('.', '').lower()
	                                                                                   # Formatting theatre name as required
            url = "http://in.bookmyshow.com/buytickets/" + name + "/cinema-" + code + "-" + theatre['VenueCode'] + "-MT/"
            url = url.replace(',', '').replace('\'', '')
	                                                                                   # url for specific theatre
            cinemas += ((name, url, theatre['VenueAddress'], city[0]), )                   # Appending details of new theatre into list

            # Just in case latitude and longitude of a theatre are requires 'VenueLongitude' and 'VenueLatitude' are keys in theatre
    except :

        pass
