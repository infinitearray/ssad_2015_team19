#! /usr/bin/env python

from requirements import get, Soup, loads, msplit, sub, cursor, system

date = raw_input("Enter Date: ")
cinema = raw_input("Enter Cinema: ")
cinema = cinema.title()
# Confirming name of cinema
sql = "SELECT url,name,city FROM cinema where name like \"%" + cinema + "%\""
cursor.execute(sql)
results = cursor.fetchall()
index = 1
for result in results :
    print index, ")", result[1]
    index += 1
option = input("Select the option: ")
cinema_url = results[option-1][0] + date
cinema_city = results[option-1][2]
movie = raw_input("Enter Movie: ")
movie = movie.title()
sql = "SELECT url,name FROM movie where name like \"%" + movie + "%\" and url like \"%" + cinema_city + "%\""
cursor.execute(sql)
results = cursor.fetchall()
index = 1
for result in results :
    print index, ")", result[1]
    index += 1
option = input("Select the option: ")
movie_url = results[option-1][0] + date
movie_url = movie_url.replace('-','').lower()

system("phantomjs new.js '" + cinema_url + "' 2>/dev/null")
page=open('./page.html').read()
html=Soup(page,"lxml")

for li in html.findAll('li', {'class' : 'list'}) :
	url1 = ""
	for a in li.findAll('a', {'class' : 'nameSpan'}) :
		url1 = "http://in.bookmyshow.com" + a['href']
	if url1 == "" :
		break
	lis = url1.split('/')
	lis[-3] += cinema_city
	url1 = '/'.join(lis)
	url1 = url1.replace('-', '').lower()
	if url1 != movie_url :
		continue

	for div in li.findAll('div', {'class' : 'body   __cn-list'}) :
		for div2 in div.findAll('div', {'class' : '_available', 'data-oline' : 'Y'}) :
			print "================================================================="
			for a in div2.findAll('a', {'class' : 'data-enabled'}) :
				print "Time: ", a.text
			for div3 in div2.findAll('div', {'class' : '__body'}) :
				for div4 in div3.findAll('div', {'class' : '_available'}) :
					det = " ".join(div4.text.split())
					det = det.split(' ')
					print "Cost:", det[1], "     ", "class:", ' '.join(det[2 : ])
				for div4 in div3.findAll('div', {'class' : '_filling'}) :
					det = " ".join(div4.text.split())
					det = det.split(' ')
					print "Cost:", det[1], "     ", "class:", ' '.join(det[2 :])
