#! /usr/bin/env python

from get_review import reviews, cursor
from requirements import db

# Dropping tables in case they exists

try :
    cursor.execute("DROP TABLE review")
except :
    pass

# Creating tables

cursor.execute("CREATE TABLE review(code VARCHAR(100) NOT NULL,details VARCHAR(4000) NOT NULL)")

# Inserting data into tables

try :
    cursor.executemany("INSERT INTO review (code,details) VALUES (\"%s\",\"%s\")", reviews)
    db.commit()
except :
    db.rollback()

db.close()
