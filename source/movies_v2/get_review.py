#! /usr/bin/env python

from requirements import get, loads, cursor

cursor.execute("SELECT DISTINCT(code) FROM movie")        # Querying database to get list of all movie codes
reviews = ()
movies = cursor.fetchall()

for code in movies :
    
    page_num = 0                                          # Initializing starting page of reviews

    while True :

        url = "http://in.bookmyshow.com/serv/getData.bms?cmd=GETREVIEWS&eventCode=" + code[0] + \
			"&type=UR&pageNum=" + str(page_num) + "&perPage=9&sort=LATEST"
        page = get(url)
        data = loads(page.content)                        # Loading JSON of all reviews into data
        if len(data['data']['Reviews']) == 0 :            # Iterating over pages till no reviews are present in a page

            break
        
        for i in data['data']['Reviews'] :

            reviews += ((str(code[0]), str(i['Review'].encode('ascii', 'ignore'))), )  
	    
	                                                  # Formatting review to remove unsupported characters
        
	page_num += 1
