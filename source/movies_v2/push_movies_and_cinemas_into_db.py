#! /usr/bin/env python

from get_movies_and_cinemas import movies, cinemas, cursor
from requirements import db

# Dropping tables in case they exists

try :
    cursor.execute("DROP TABLE cinema")
except :
    pass

try :
    cursor.execute("DROP TABLE movie")
except :
    pass

# Creating tables

cursor.execute("CREATE TABLE cinema (name VARCHAR(100) NOT NULL,url VARCHAR(200) NOT NULL,address VARCHAR(300) NOT NULL,city VARCHAR(60))")
cursor.execute("CREATE TABLE movie (name VARCHAR(100) NOT NULL,url VARCHAR(200) NOT NULL,dimension VARCHAR(20) NOT NULL,censor VARCHAR(20),release_date VARCHAR(20),language VARCHAR(20),code VARCHAR(30) NOT NULL)")

# Inserting data into tables

try :
    cursor.executemany("INSERT INTO cinema (name,url,address,city) VALUES (%s,%s,%s,%s)", cinemas)
    db.commit()
except :
    db.rollback()

try :
    cursor.executemany("INSERT INTO movie (name,url,dimension,censor,release_date,language,code) VALUES (%s,%s,%s,%s,%s,%s,%s)", movies)
    db.commit()
except :
    db.rollback()
db.close()
