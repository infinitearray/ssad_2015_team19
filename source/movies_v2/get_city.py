#! /usr/bin/env python

from requirements import get, msplit, loads

# Getting list of all cities available

URL  = 'http://in.bookmyshow.com/serv/getData/?cmd=GETREGIONS'       # URL to get details of all states and cities
data = get(URL).content                                              # Stores content of the page
text = msplit(';|=',data)                                            # Splitting JSON content with delimeters ; and = to get dictionary of states
regions = loads(text[1])                                             # Load the JSON content into dictionary with key as state

# Formatting data of regions as required

cities = ()

for state in regions :

    for city in regions[state] :

        name = city['name'].replace('(', '').replace(')', '').replace(' ', '-').lower()         # Formatting the name of city
        code = city['code']
        url = 'http://in.bookmyshow.com/'+name
        cities += ((name, code, state.lower(), url), )                                       # Storing name, code, state, url of the state
