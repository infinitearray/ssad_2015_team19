#! /usr/bin/env python

from requirements import get, Soup, loads, msplit, sub, cursor

city = raw_input("Enter City: ")
city = city.lower()
date = raw_input("Enter Date YYYYMMDD: ")
cinema = raw_input("Enter Cinema: ")
cinema = cinema.title()
sql = "SELECT url,name FROM cinema where city like \"%" + city + "%\" and name like \"" + cinema + "%\""
cursor.execute(sql)
results = cursor.fetchall()
index = 1

for result in results :
	print index, ")", result[1]
	index += 1

option = input("Select the option: ") 
url = results[option-1][0] + date
html = Soup(get(url).content, "lxml")

for li in html.findAll('li', {'class' : 'list'}) :
    for div in li.findAll('div', {'class' : 'details'}) :
	for a in div.findAll('a', {'class' : '__name'}) :
		print a.text.strip(), "http://in.bookmyshow.com" + a['href']
