#! /usr/bin/env python

from get_classes_of_cinemas import classes, cursor
from requirements import db

# Dropping tables in case they exists

try :
    cursor.execute("DROP TABLE classes")
except :
    pass

# Creating tables

cursor.execute("CREATE TABLE classes (name VARCHAR(300) NOT NULL,class VARCHAR(20) NOT NULL,code VARCHAR(2) NOT NULL,price VARCHAR(10) NOT NULL)")

# Inserting data into tables

for part in classes:
	try :
    		
		cursor.execute("INSERT INTO classes (name,class,code,price) VALUES (\"%s\",\"%s\",\"%s\",\"%s\")"%(part[0],part[1],part[2],part[3]))
    		db.commit()
	except :
    		db.rollback()

db.close()

