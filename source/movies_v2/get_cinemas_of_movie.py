#! /usr/bin/env python

from requirements import get, loads, Soup, msplit, sub, cursor

city = raw_input("Enter City: ")
city = city.lower()
movie = raw_input("Enter Movie: ")
movie = movie.title()
date = raw_input("Enter Date YYYYMMDD: ")

sql = "SELECT url,name FROM movie where name like \"%" + movie + "%\" and url like \"%" + city + "%\""
cursor.execute(sql)                                                     # Querying database to get url of required movies in required city
results = cursor.fetchall()
index = 1
# Displaying all results
for result in results :
    print index, ")", result[1]
    index += 1
# prompting user to select one option
option=input("Select the option: ")

url = results[option-1][0] + date                                      # generating url for selected movie
html = Soup(get(url).content, "lxml")

flag = True

for div in html.findAll('div', {'class' : 'details'}) :

    for div2 in div.findAll('div', {'class' : '__name'}) :

        for a in div2.findAll('a', {'class' : '__venue-name'}) :

		if a['href'][-8 : ] == date :                             # Checking if date in  result is required date 
			flag = False
			print a.text.strip(), "http://in.bookmyshow.com" + a['href'][ : -8] + "/" + a['href'][-8 : ]

if flag :
	print "No results matching your search" 
