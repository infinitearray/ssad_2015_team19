from requests import get
from pickle import load,dump
from bs4 import BeautifulSoup as Soup

with open('cities_list.pickle', 'rb') as handle:

    cities = load(handle)

with open('cinemas_list.pickle', 'rb') as handle:

    cinemas = load(handle)

def page(codettr,codecin,code,reqdate):
        url = "http://in.bookmyshow.com/buytickets/"+codettr+"/cinema-"+code+"-"+codecin+"-MT/"+reqdate
	page = get(url)
	return Soup(page.content,'lxml')

def getcontent(html):

    cinemas=()
    times=()

    for tag in html.findAll('span',{'class':'bold'}):
        for name in tag.findAll('a'):
            cinemas+=((name.text,"in.bookmyshow.com"+name['href']),)

    for tag in html.findAll('span',{'class':'shtime'}):
        for name in tag.findAll('a',{'class':'bytclick'}):
            times+=(name.text,)
            print times[-1],tag['data-online']
        lis=raw_input()
    
    return cinemas[:len(cinemas)/2]

reqdate='20151006'
#reqdate=raw_input("Date ?? ")

theatres={}

for city in cities:

    code=cities[city].lower()

    theatres[city]=()

    for location in cinemas[city]:

        codecin=location[0]
        codettr=location[2].replace(':','').replace(' ','-').replace('\\','').lower()
        theatres[city]+=((codettr,getcontent(page(codettr,codecin,code,reqdate))),)
   
    #for xyz in theatres[city]:
    #    print "-----------",xyz[0],"---------------"
    #    for de in xyz[1]:
    #        print de
    #print theatres[city]
    
    #lis=raw_input("Next-city")

with open('theatres_list'+reqdate+'.pickle','wb') as handle:
        dump(theatres,handle)
