#! /usr/bin/env python

from selenium import webdriver
from string import ascii_lowercase as alph
from bs4 import BeautifulSoup as soup
from pickle import dump

driver=webdriver.Firefox()
driver.get("http://in.bookmyshow.com")
html=soup(driver.page_source)
cities={}

for block in html.find_all('option'):
    if block["value"]!="":
        cities[block.text]=block["value"]

driver.close()

with open('cities_list.pickle','wb') as handle:
    dump(cities,handle)

