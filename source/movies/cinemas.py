#! /usr/bin/env python

from selenium import webdriver
from bs4 import BeautifulSoup as soup
from pickle import load

locations={}
with open('cities_list.pickle', 'rb') as handle:
      cities = load(handle)
driver=webdriver.Firefox()
count=0
for city in cities:
    locations[city]=[]
    count+=1
    print city,count,len(cities)

    url="http://in.bookmyshow.com/"+city.replace(')','').replace('(','').replace(' ','-').lower()+"/movies"
    driver.get(url)
    html=soup(driver.page_source)
    for block in html.find_all("div",{'class':'wrap'}):
        for block2 in block.find_all("ul"):
            for block3 in block2.find_all("li"):
                if block3["data-value"]!="":
                    locations[city].append(block3.text)
    print "hi"                
driver.close()

for city in cities:
    print city,locations[city]
