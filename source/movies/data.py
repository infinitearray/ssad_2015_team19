import sqlite3 as sq

from pickle import load

with open('cities_list.pickle', 'rb') as handle:
    cities = load(handle)

with open('cinemas_list.pickle', 'rb') as handle:
    locations = load(handle)

with open('theatres_list20151006.pickle', 'rb') as handle:
    theatres = load(handle)


conn = sq.connect("movies.db")

with conn:

    c = conn.cursor()
    c.execute("DROP TABLE IF EXISTS cities;")
    c.execute("DROP TABLE IF EXISTS locations;")
    c.execute("DROP TABLE IF EXISTS cinemas;")

    sql = "CREATE TABLE cities (city VARCHAR(20) NOT NULL,code VARCHAR(6) NOT NULL);"
    c.execute(sql)

    sql = "CREATE TABLE locations (city VARCHAR(20) NOT NULL,location VARCHAR(20) NOT NULL,code VARCHAR(6) NOT NULL);"
    c.execute(sql)

    sql = "CREATE TABLE cinemas (city VARCHAR(20) NOT NULL,theatre VARCHAR(20) NOT NULL,movie VARCHAR(6) NOT NULL);"
    c.execute(sql)
    
    for city in cities:

        c.execute("INSERT INTO cities (city,code) VALUES (?,?);",(city,cities[city]))

        for location in locations[city]:

            c.execute("INSERT INTO locations (city,location,code) VALUES (?,?,?);",(city,location[2],location[0]))
    
    for city in theatres:
        for ttr in theatres[city]:
            for cinemas in ttr[1]:
                c.execute("INSERT INTO cinemas (city,theatre,movie) VALUES (?,?,?);",(city,ttr[0],cinemas[0]))
                #print city,ttr[0],movies[0]
