import requests
import os
from bs4 import BeautifulSoup

def getMobileList():

    """
    Lists all mobiles from mysmartprice.com
    Returns a list of mobile phones from the search result
    Each element in the list is a dictionary containing the following keys:
        name : Name of the phone
        id : ID of the on mysmartprice.com
        link : url to the page of the phone. Can be obtained from name and id
        price : best price of mobile
    """

    pageNum=1
    morePages=True
    phoneList=[]
    #get result from all pages
    while morePages:
        morePages=False
        if pageNum==1:
            fquery="http://www.mysmartprice.com/mobile/pricelist/mobile-price-list-in-india.html#subcategory=mobile"
        else:
            fquery="http://www.mysmartprice.com/mobile/pricelist/pages/mobile-price-list-in-india-"+str(pageNum)+".html#subcategory=mobile"
        os.system("phantomjs phantomjsGetPage.js '"+fquery+"' >/dev/null")
        page=open('./jshtml/page.html')
        soup=BeautifulSoup(page.read())
        for phone in soup.findAll('div',{'class':'msplistitem'}):
            morePages=True
            try:
                pinfo=dict()
                pinfo['name']=str(phone.find('a',{'class':'item-title'}).text).strip()
                pinfo['id']=int(str(phone.get('data-mspid')))
                pinfo['link']=str(phone.find('a',{'class':'item-title'}).get('href')).strip()
                pinfo['price']=str(phone.find('span',{'class':'price-val'}).text).strip()
                #append the information to phone list
                phoneList.append(pinfo)
                
            except:
                print 'failed'
                print pinfo
                pass
        
        #go to next page
        pageNum+=1
                
    return phoneList



def searchMobile(query):

    """
    Searches of mobiles from a name
    Returns a list of mobile phones from the search result
    Each element in the list is a dctionary containing the following keys:
        name : Name of the phone
        id : ID of the on mysmartprice.com
        image : Link to photo of the phone
        link : url to the page of the phone. Can be obtained from name and id
        rating : Rating of the phone(out of 5)
        price : Cost of the phone(Best among all available prices from different stores)
        mrp : Actual price of the product(Without discount)
        discount : Discount applied
        info : Brief specification
    """
    query=query.replace(' ','+')
    pageNum=1
    morePages=True
    phoneList=[]
    #get result from all pages
    while morePages:
        morePages=False
        fquery="http://www.mysmartprice.com/mobile/pricelist/mobile-price-list-in-india.html#subcategory=mobile&s="+query+"&page="+str(pageNum)
        os.system("phantomjs phantomjsGetPage.js '"+fquery+"' >/dev/null")
        page=open('./jshtml/page.html')
        soup=BeautifulSoup(page.read())

        #remove the recommendations
        try:
            soup.find('div',{'class':'highlight recommendations'}).decompose()
        except AttributeError:
            pass
        for phone in soup.findAll('div',{'class':'msplistitem'}):
            morePages=True
            try:
                pinfo=dict()
                pinfo['name']=str(phone.find('a',{'class':'item-title'}).text).strip()
                pinfo['id']=int(str(phone.get('data-mspid')))
                pinfo['image']=str(str(phone.find('a',{'class':'imgcont'}).find('img').get('src'))).strip()
                pinfo['link']=str(phone.find('a',{'class':'item-title'}).get('href')).strip()
                pinfo['rating']=str(phone.find('div',{'class':'rating-input callout-target'}).get('data-callout'))[6:-15].strip()
                pinfo['price']=str(phone.find('span',{'class':'price-val'}).text).strip()
                #fetch the discount if present
                try:
                    pinfo['mrp']=str(phone.find('span',{'class':'price-mrp'}).text).strip()
                    pinfo['discount']=str(phone.find('span',{'class':'percentage-saved'}).text)[1:-1].strip()
                except:
                    pinfo['mrp']=pinfo['price']
                    pinfo['discount']='0% Off'                
                pinfo['info']=[str(x.text).strip() for x in phone.findAll('li',{'class':'info-item'})]
                #append the information to phone list
                phoneList.append(pinfo)
                
            except:
                print 'failed'
                print pinfo
                pass
        
        #go to next page
        pageNum+=1
                
    return phoneList


def getPhoneSpecs(url):

    """
    Gets specifications of a phone from url
    returns a dictionary, which contains dictionaries of information about a particular feature, addressed by the feature
    Eg. It returns a dictionary having Design and Build,Software,Display,Camera,Storage,Battery,Network and Connectivity,Platform,Sound.
    **The feature names are lower-cased
        dict['storage']={
            'Internal' : '8 GB, 1 GB RAM'
            'Expandable' : 'microSD, up to 32 GB'
        }
    """

    toret=dict()
    #Fetching link to specifications tab
    try:
        data=requests.get(url).text
    except:
        print('Error retrieving page')
        return toret
    soup=BeautifulSoup(data)
    url='http://www.mysmartprice.com'+str(soup.find('a',{'id':'tab_spec'}).get('href')).strip()

    #fetching the page
    try:
        data=requests.get(url).text
        soup=BeautifulSoup(data)
    except:
        print('Error retrieving page')
        return toret

    #Add basic info
    toret['image']=soup.find('div',{'class':'product_img'}).find('img').get('src')
    product_title=soup.find('div',{'class':'product_title'})
    toret['name']=str(product_title.find('span',{'itemprop':'name'}).text).strip()
    toret['brand']=str(product_title.find('span',{'itemprop':'brand'}).text).strip()
    toret['mid']=str(product_title.find('h1').get('data-mspid')).strip()

    #Add data from specification table
    presentSpec=None
    for row in soup.find('table').findAll('tr'):
        if row.find('td')==None:
            #change the feature being specified
            try:
                presentSpec=str(row.find('th').text).strip().lower()
                toret[presentSpec]=dict()
            except:
                print "Couldn't switch feature",row.find('th').text
        else:
            #specify the feature
            try:
                toret[presentSpec][str(row.find('td',{'class':'mspkey'}).text).strip().lower()]=str(row.find('td',{'class':'mspvalue'}).text).strip()
            except:
                try:
                    toret[presentSpec][str(row.find('td',{'class':'mspkey'}).text.encode('ascii','replace')).strip().lower()]=str(row.find('td',{'class':'mspvalue'}).text.encode('ascii','replace')).strip()
                except:
                    print "Error reading feature",row.text
                
    return toret

def getPhonePrice(url):

    """
    Takes the url of the phone as input and returns a list of vendors selling the product and their information
    Each element of the list is a dictionary containing the following keys :
        name : Name of the vendor 
        image : Image(logo) of the vendor
        price : Cost of the product as specified by the vendor
        rating : rating out of 100%
        delivery : If the item is in stock, it shows the estimated delivery time
        shipping_cost : cost of shipping
        emi : True if EMI is available
        return : Days mentioned in return policy
        cod : True if cash on delivery is available
        link : url to buy the product
    """

    priceList=[]
    #fetching the page
    try:
        data=requests.get(url).text
    except:
        print('Error retrieving page')
        return priceList
    #creating soup
    soup=BeautifulSoup(data)

    priceTable=soup.find('div',{'id':'pricetable'})
    for store in priceTable.findAll('div',{'class':'store_pricetable'}):
        try:
            priceInfo=dict()
            priceInfo['name']=str(store.find('div',{'class':'store_img'}).find('img').get('alt')).strip()
            priceInfo['image']=str(store.find('div',{'class':'store_img'}).find('img').get('src')).strip()
            priceInfo['price']=int(str(store.find('div',{'class':'store_price'}).text[1:]).replace(',','').strip())
            priceInfo['rating']=str(store.find('div',{'class':'store_rating_bar_out callout-target'}).get('data-callout')).strip()
            priceInfo['delivery']=str(store.find('div',{'class':'store_delivery'}).text)[10:].strip()
            sp_info=store.find('div',{'class':'store_payment'})
            sp_info=sp_info.findNext('div')
            if str(sp_info.get('class')[2]).strip().endswith('on'):
                priceInfo['emi']=True
            else:
                priceInfo['emi']=False
            sp_info=sp_info.findNext('div')
            priceInfo['return']=str(sp_info.text).split(':')[1].strip()
            sp_info=sp_info.findNext('div')
            if str(sp_info.get('class')[2]).strip().endswith('on'):
                priceInfo['cod']=True
            else:
                priceInfo['cod']=False
            priceInfo['link']=str(store.find('div',{'class':'store_gostore'}).findNext('div').get('data-url')).strip()
            try:
                ship_price=store.find('div',{'class':'store_shipping'}).text.encode('ascii','ignore')
            except:
                ship_price="Free Shipping"
            if 'free' in ship_price.lower():
                priceInfo['shipping_cost']='0'
            else:
                priceInfo['shipping_cost']=ship_price.replace('Shipping Charges ','').strip()
            #append the dictionary to the list
            priceList.append(priceInfo)

        except:
            print 'failed'
            print priceInfo
            pass
        
    return priceList
