from dbconfig import read_db_config
import mobile_crawl
import MySQLdb as mdb

def getConnection():
    db_config=read_db_config()
    try:
        conn=mdb.connect(db_config['host'],db_config['user'],db_config['password'],db_config['database'])
        return conn
    except:
        return None

def setupTables():
    conn=getConnection()
    with conn:
        cur=conn.cursor()
        cur.execute("CREATE TABLE Mobile( id int NOT NULL PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL, image varchar(255), mobile_id int NOT NULL, brand varchar(255), phone_size varchar(255), weight varchar(255), os varchar(255), screen_resolution varchar(255), screen_size varchar(255), screen_type varchar(255), primary_camera varchar(255), front_camera varchar(255), video varchar(255), camera_features text, internal_storage varchar(255), external_storage varchar(255), battery_capacity varchar(255), battery_type varchar(255), talk_time varchar(255), standby_time varchar(255), 2g varchar(255), 3g varchar(255), 4g varchar(255), wifi varchar(255), bluetooth varchar(255), gps varchar(255), nfc varchar(255), wired varchar(255), radio varchar(255), processor varchar(255), cpu varchar(255), gpu varchar(255), sensors text, speakers varchar(255), headphone varchar(255));")
        cur.execute("CREATE TABLE Mobile_Vendor(id int NOT NULL PRIMARY KEY AUTO_INCREMENT, mobile_id int NOT NULL, vendor_id int,price int NOT NULL, shipping_cost varchar(255), return_days varchar(255), cod varchar(255), emi varchar(255), delivery_days varchar(255), purchase_url varchar(255));")
        cur.execute("CREATE TABLE Vendor(id int NOT NULL PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT NULL, image  varchar(255), rating varchar(255));")

def addMobile(link,conn=None):
    mSpec=mobile_crawl.getPhoneSpecs(link)
    check_query="SELECT * FROM Mobile WHERE mobile_id=%s"
    if conn==None:
        conn=getConnection()
    with conn:
        cur=conn.cursor()
        cur.execute(check_query,mSpec['mid'])

        if cur.rowcount==0:
            mid=addMobiletoDB(mSpec,conn)
            mPrice=mobile_crawl.getPhonePrice(link)
            for vendor in mPrice:
                vid=addVendortoDB(vendor,conn)
                addMobileVendor(vendor,mid,vid,conn)

def addMobileVendor(vInfo,mID,vID,conn=getConnection()):
    "Joins Mobile and Vendor with vendor info in DB"
    query="INSERT INTO Mobile_Vendor(mobile_id, vendor_id, price, shipping_cost, return_days, cod, emi, delivery_days, purchase_url) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"
    row=[]
    row.append(mID)
    row.append(vID)
    row.append(vInfo['price'])
    row.append(vInfo['shipping_cost'])
    row.append(vInfo['return'])
    row.append(vInfo['cod'])
    row.append(vInfo['emi'])
    row.append(vInfo['delivery'])
    row.append(vInfo['link'])
    with conn:
        cur=conn.cursor()
        cur.execute(query,row)

def addVendortoDB(vInfo,conn=getConnection()):
    """takes name and rating as vInfo and adds it to the database if not already present"""
    query="INSERT INTO Vendor(name, image, rating) VALUES (%s, %s, %s)"
    check_query="SELECT * FROM Vendor WHERE name=%s"
    row=[]
    row.append(vInfo['name'])
    row.append(vInfo['image'])
    row.append(vInfo['rating'])
    with conn:
        cur=conn.cursor()
        cur.execute(check_query,(vInfo['name'],))
        
        if cur.rowcount==0:
            cur.execute(query,row)
            cur.execute(check_query,(vInfo['name'],))
        
        assert cur.rowcount==1
        return cur.fetchall()[0][0]

def addMobiletoDB(mInfo,conn=getConnection()):
    query = "INSERT INTO Mobile(name , image , mobile_id, brand, phone_size , weight , os , screen_resolution , screen_size , screen_type , primary_camera , front_camera , video , camera_features , internal_storage , external_storage , battery_capacity , battery_type , talk_time , standby_time , 2g , 3g , 4g , wifi , bluetooth , gps , nfc , wired , radio , processor , cpu , gpu , sensors, speakers , headphone ) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
    dbcol=[]

    #make column to insert into database

    #name
    dbcol.append(mInfo['name'])
    #image url
    try:
        dbcol.append(mInfo['image'])
    except:
        dbcol.append(None)
    #model id
    dbcol.append(mInfo['mid'])
    #brand
    try:
        dbcol.append(mInfo['brand'])
    except:
        dbcol.append(None)
    #phone size
    try:
        dbcol.append(mInfo['design and build']['dimensions'])
    except:
        dbcol.append(None)
    #weight
    try:
        dbcol.append(mInfo['design and build']['weight'])
    except:
        dbcol.append(None)
    #os
    try:
        dbcol.append(mInfo['software']['operating system'])
    except:
        dbcol.append(None)
    #screen resolution
    try:
        dbcol.append(mInfo['display']['resolution'])
    except:
        dbcol.append(None)
    #screen size
    try:
        dbcol.append(mInfo['display']['size'])
    except:
        dbcol.append(None)
    #screen type
    try:
        dbcol.append(mInfo['display']['type'])
    except:
        dbcol.append(None)
    #primary camera
    try:
        dbcol.append(mInfo['camera']['primary'])
    except:
        dbcol.append(None)
    #front camera
    try:
        dbcol.append(mInfo['camera']['front-facing'])
    except:
        dbcol.append(None)
    #video
    try:
        dbcol.append(mInfo['camera']['video'])
    except:
        dbcol.append(None)
    #camera features
    try:
        dbcol.append(mInfo['camera']['features'])
    except:
        dbcol.append(None)
    #internal storage
    try:
        dbcol.append(mInfo['storage']['internal'])
    except:
        dbcol.append(None)
    #external storage
    try:
        dbcol.append(mInfo['storage']['external'])
    except:
        dbcol.append(None)
    #battery capacity
    try:
        dbcol.append(mInfo['battery']['capacity'])
    except:
        dbcol.append(None)
    #battery type
    try:
        dbcol.append(mInfo['battery']['type'])
    except:
        dbcol.append(None)
    #talktime
    try:
        dbcol.append(mInfo['battery']['talk time'])
    except:
        dbcol.append(None)
    #standby time
    try:
        dbcol.append(mInfo['battery']['standby time'])
    except:
        dbcol.append(None)
    #2g
    try:
        dbcol.append(mInfo['network and connectivity']['2g'])
    except:
        dbcol.append(None)
    #3g
    try:
        dbcol.append(mInfo['network and connectivity']['3g'])
    except:
        dbcol.append(None)
    #4g
    try:
        dbcol.append(mInfo['network and connectivity']['4g'])
    except:
        dbcol.append(None)
    #wifi
    try:
        dbcol.append(mInfo['network and connectivity']['wi-fi'])
    except:
        dbcol.append(None)
    #bluetooth
    try:
        dbcol.append(mInfo['network and connectivity']['bluetooth'])
    except:
        dbcol.append(None)
    #gps
    try:
        dbcol.append(mInfo['network and connectivity']['gps'])
    except:
        dbcol.append(None)
    #nfc
    try:
        dbcol.append(mInfo['network and connectivity']['nfc'])
    except:
        dbcol.append(None)
    #wired
    try:
        dbcol.append(mInfo['network and connectivity']['wired'])
    except:
        dbcol.append(None)
    #radio
    try:
        dbcol.append(mInfo['network and connectivity']['radio'])
    except:
        dbcol.append(None)
    #processor
    try:
        dbcol.append(mInfo['platform']['processor'])
    except:
        dbcol.append(None)
    #cpu
    try:
        dbcol.append(mInfo['platform']['cpu'])
    except:
        dbcol.append(None)
    #gpu
    try:
        dbcol.append(mInfo['platform']['gpu'])
    except:
        dbcol.append(None)
    #sensors
    try:
        dbcol.append(mInfo['platform']['sensors'])
    except:
        dbcol.append(None)
    #speakers
    try:
        dbcol.append(mInfo['sound']['loudspeaker'])
    except:
        dbcol.append(None)
    #headphone
    try:
        dbcol.append(mInfo['sound']['headphones'])
    except:
        dbcol.append(None)

    #insert the extracted information into db
    with conn:
        cur=conn.cursor()
        cur.execute(query,dbcol)
        cur.execute('SELECT * FROM Mobile WHERE mobile_id=%s',(mInfo['mid'],))
        assert cur.rowcount==1
        return cur.fetchall()[0][0]
