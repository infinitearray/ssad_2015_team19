## Date and time
August 13 2015 , 6:00 PM

## Attendees
Vivek Nynaru,Bandi Harsha Vardhan,Vedant,Koushik Sane

## Agenda
Explanation of the database for each website

## Minutes
1 hour

### Discussion
*(whatever the clients has explained, what all doubts students asked, and solutions proposed.)*
* Explained about the schema of databse
 * For each website the schema is explained


### Action Points
* Get an overview of the database
 * See the database schema sent through the mail
 * Observe the data to be extracted in the webpage
* Try to view a sample webpage of the websitte allotted and extrac some data using basic BeautifulSoup
 * Learn about BeautifulSoup
 * See some examples

## Date of the next meeting
August 20 2015 , 6:00 PM
