Date and Time:
October 05 2015 , 5:00 PM

Attendees:
Vivek Nynaru,Bandi Harsha Vardhan,Vedant,Koushik Sane

Agenda:
Discussion on R1 and also on the progress in the work.

Minutes:
30 Minutes

Discussion:
	1. Regarding the use of other websites for getting all books.
	2. Suggestions for crawling Bigbasket for getting all subcategories and all the products in it.
	3. General suggestions for R1.

Action Points:
	1. Meeting TA to get suggestions for the presentation.
	2. Learnt how to put data into the database.
	3. Learnt how to get data loaded by an AJAX call
