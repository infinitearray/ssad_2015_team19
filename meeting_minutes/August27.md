## Date and time
August 27 2015 , 5:50 PM

## Attendees
Bandi Harsha Vardhan,Vedant, Vivek Nynaru,Koushik Sane

## Agenda
Reviewed the process and codes for crawling data and gave guidelines for further progress 

## Minutes
30 minutes

### Discussion
* Explained how to extract data from BeautifulSoup into python objects
 * Explained how to use phantomJS to load the whole page after AJAX calls.


### Action Points
* Examine the structure of webpages of the website so crawling using html tags is possible.
 * phantomJS sample code sent on mail was used for a better understanding of how it works. 
* Use phantomJS to load the complete webpage.
 * Crawl more pages to extract greater amount of data per product.

