## Date and time
August 10 2015 6:00 PM

## Attendees
Vivek Nynaru,Bandi Harsha Vardhan,Vedant,Koushik Sane

## Agenda
Introduction to project and division of categories

## Minutes
20 Minutes

### Discussion
Client Explained Some part of the previoulsy written code
We asked about the system requirements

### Action Points
To get a grip over Beautiful Soup and urllib modules in python

* Learn Beautiful Soup
 * Analysing the functions in that module
 * Writing a simple code to extract html of a page
  * Getting data inside specified tags
* Learn Urllib
 * Get a webpage with specified url


## Date of the next meeting
August 13 2015 6:00 PM
