## Date and time
September 03 2015 , 6:00 PM

## Attendees
Vivek Nynaru,Bandi Harsha Vardhan,Vedant,Koushik Sane

## Agenda
Clarification of doubts and Suggestions from mentor for every category

## Minutes
1 hour

### Discussion
* Suggestions for crawling webpage after AJAX calls and javascript calls
* Sugestions for crawling Mysmartprice for getting list of all genres and books
* Suggestions for crawling BookMyshow for getting cinemas and movie names


### Action Points

* Meeting TA to get suggestions for extracting data loaded by AJAX calls
* Learnt how to get data loaded by an AJAX call
* Extract data from Bookmyshow loaded by an AJAX call
* Testing Alternate ways to get list of books from Mysmartprice 
