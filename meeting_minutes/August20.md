## Date and time
August 20 2015 , 6:00 PM

## Attendees
Vivek Nynaru,Bandi Harsha Vardhan,Vedant,Koushik Sane

## Agenda
Explanation of the softwares used for crawling data efficiently from each website

## Minutes
1 hour

### Discussion
*(whatever the clients has explained, what all doubts students asked, and solutions proposed.)*
* Explained about the softwares used for crawling data.
 * Gave an overview about how phantomJS can be used for getting the job done easily.


### Action Points
* Get an idea about the softwares used for crawling data from different websites.
 * phantomJS sample code sent on mail was used for a better understanding of how it works. 
* Try to get the data using phantomJS and any other softwares mentioned in the discussion.
 * Learn about PhantomJS
 * Try to use phantomJS on some sites.

