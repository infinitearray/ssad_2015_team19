## Date and time
September  2015 , 6:00 PM

## Attendees
Vivek Nynaru,Bandi Harsha Vardhan,Vedant,Koushik Sane

## Agenda
Clarification of doubts and Suggestions from mentor for every category

## Minutes
1 hour

### Discussion
* Suggestions for crawling webpage after AJAX calls and javascript calls using phantomjs
* Mentor asked to crawl data for books from a different website, flipkart.com
* Suggestions for crawling data from bigbasket.com for categories of groceries
* Suggestions for crawling data from bookmyshow.com regarding movies,cities and theatres


### Action Points

* Crawl basic list of categories in books from flipkart.com
* Extract data from bigbasket.com after solving the city issue
* Try to extract more details for mobiles and movies after making sure of things done
