### Students' Information
201425135, 201401197, 201401027, 201401033

-----------------

### Feedback of documents

#### Project Concept
(Marks given : 7/10)

* Explaination of profile of users insufficient.
* Diagram or some text, which can display team's understanding of project.

#### Project Plan
(Marks given : 9/10)

* Dates are missing in schedule

#### Status Tracker 1
(Marks given : 5/5)

* Estimated time, Actual Time spend, etc in "Individual Status" tab, are not numeric. Change it to 2 or 120 instead of "2hrs"

#### Status Tracker 2
(Marks given : 5/5)

* Same as status tracker 2

#### SRS
(Marks given : 28/30)

* Good work in SRS
* 1 marks has been cut because of improper team information table.
* 1 marks for User profile description.

#### Test Plan
(Marks given : 8/10)

* Test description needed steps to be followed in that particular test case, not the functioning. 

#### Status Tracker 3
(Marks given : 4/5)

* Feedback of Status Tracker 1 and 2 not completed. (was also previously shared with team on Facebook chat)

#### Status Tracker 4
(Marks given : 4/5)

* Feedback of Status Tracker 1 and 2 not completed. (was also previously shared with team on Facebook chat)
